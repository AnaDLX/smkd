import React, { Component } from 'react';
import './style/css/Content.css';

// IMG
import arrow from './style/img/arrow.svg';

// MATERIAL UI
import Checkbox from '@material-ui/core/Checkbox';
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import {Dialog} from "@material-ui/core";

class Nade extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nadePosition: {
        left: this.props.x,
        top: this.props.y,
      },
    }
  }

  handleOpen = () => { this.setState({ open: true }) };
  handleClose = () => { this.setState({ open: false }) };

  render() {
    return (
      <MuiThemeProvider theme={this.props.theme}>
        <div className="nade" style={this.state.nadePosition} onClick={this.handleOpen}/>
        <Dialog open={this.state.open}>
          <div>
            Lol
          </div>
        </Dialog>
      </MuiThemeProvider>
    )
  }
}

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      smokeChecked: true,
      flashChecked: true,
      moloChecked: true,
      heChecked: true,
      boostChecked: false,
      map: {
        name: "Cache",
        grenades: [
          {_id: '', pos: [265, 226], type: 'SMOKE', favorite: 'BOOL'},
          {pos: [370, 391]}
        ],
      }
    }
  }

  handleCheckboxChange = name => event => {
    this.setState({[name]: event.target.checked });
  };

  getMap = (mapName) => {
    // Fetch la map(mapName) -> map
  };

  render() {
    return (
        <MuiThemeProvider theme={this.props.theme}>
          <div className={this.props.shrink ? "Content ContentShrinked" : "Content"}>
          <h1>{ this.props.selectedMap.name }</h1>
          <div className="content-content">
            <div className="filters">
              <h2>Filters</h2>
              <div className="filter-category">
                <h3><img src={arrow} alt="SMKD Arrow" />Type</h3>
                <ul>
                  <li>
                    <Checkbox value="smokeChecked" color="primary"
                      checked={this.state.smokeChecked}
                      onChange={this.handleCheckboxChange('smokeChecked')}
                    />
                    <p>Smoke</p>
                  </li>
                  <li>
                    <Checkbox value="flashChecked" color="primary"
                        checked={this.state.flashChecked}
                        onChange={this.handleCheckboxChange('flashChecked')}
                    />
                    <p>Flash</p>
                  </li>
                  <li>
                    <Checkbox value="moloChecked" color="primary"
                        checked={this.state.moloChecked}
                        onChange={this.handleCheckboxChange('moloChecked')}
                    />
                    <p>Molotov</p>
                  </li>
                  <li>
                    <Checkbox value="heChecked" color="primary"
                        checked={this.state.heChecked}
                        onChange={this.handleCheckboxChange('heChecked')}
                    />
                    <p>Explosive</p>
                  </li>
                  <li>
                    <Checkbox value="boostChecked" color="primary"
                        checked={this.state.boostChecked}
                        onChange={this.handleCheckboxChange('boostChecked')}
                    />
                    <p>Boosts</p>
                  </li>
                </ul>
              </div>
            </div>
            <div className="map-container">
              <img className="map" src={require('./style/img/' + this.props.selectedMap.name.toLowerCase().replace(/\s/g, '') + 'Radar.webp')} alt={'SMKD ' + this.props.selectedMap.name + ' Radar'} />
              {
                this.state.map.grenades.map((item, index) => (
                  <Nade
                    x={item.pos[0]}
                    y={item.pos[1]}
                    key={index}
                    theme={this.props.theme}
                  />
                ))
              }
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default Content;
