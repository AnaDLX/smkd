import React, {Component} from 'react';

// Img
import logo from "./style/img/smkd_logo3.png";
import steamLogo from "./style/img/steamLogo.png";
import down_arrow from "./style/img/down-arrow.svg";

// Material UI
import {MuiThemeProviderOld} from "@material-ui/core/es/styles/MuiThemeProvider";
import createMuiTheme from "@material-ui/core/es/styles/createMuiTheme";
import Dialog from "@material-ui/core/Dialog";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {main: '#0073aa'},
    secondary: {A400: '#2E2E2E'},
  }
});

class LoginDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      loginUsername: '',
      loginPassword: '',
      signupUsername: '',
      signupEmail: '',
      signupPassword: '',
      signupPasswordConf: '',
      displayLogin: true,
      user : {},
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.user !== prevProps.user) {
      this.setState({user: this.props.user});
    }
  }

  steamLogin = () => {
    window.location.href = "http://localhost:8080/login/steam";
  };

  login = () => {
    this.setState({loginErr: ''});
    if (this.state.loginUsername === '' || this.state.loginPassword === '')
      this.setState({loginErr: 'Please provide all the informations.'});
    else {
      // Send data here, check response and edit loginErr in case
      console.log(this.state.loginUsername + ' ' + this.state.loginPassword);
    }
  };

  signup = () => {
    this.setState({signupErr: ''});
    if (this.state.signupUsername === '' || this.state.signupPassword === '' || this.state.signupEmail === '' || this.state.signupPasswordConf === '')
      this.setState({signupErr: 'Please provide all the informations.'});
    else if (this.state.signupUsername.length < 5)
      this.setState({signupErr: 'Username must be at least 5 characters.'});
    else if (this.state.signupPassword.length < 7)
      this.setState({signupErr: 'Password must have at least 7 characters.'});
    else if (this.state.signupPassword !== this.state.signupPasswordConf)
      this.setState({signupErr: 'Passwords do not match.'});
    else {
      // Send data here, check response and edit signupErr in case
      const {signupUsername, signupPassword, signupEmail} = this.state;
      const userSignUp = {
        username: signupUsername,
        password: signupPassword,
        email: signupEmail,
        img_url: '',
      };

      fetch('http://localhost:8080/users', {
        method: "POST",
        body: JSON.stringify(userSignUp),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
      }).then(function (response) {
        const responseJSON = response.json();

        responseJSON.then(function (value) {
          if (value.errors !== undefined) {
            // Display the global error message
            console.log(value.message);
            // this.setState({signupErr: value.message});

            // Display each error message returned by the server
            // Object.keys(value.errors).forEach(function(error) {
            //   console.log(value.errors[error]);
            // });
          } else {
            console.log("user: ");
            console.log(value); // Retour de l'utilisateur récemment inscrit
          }
        })
      }, function (error) {
        console.log(error.message);
      })

    }
  };

  handleClickOpen = () => { this.setState({open: true}) };
  handleClose = () => { this.setState({open: false}) };

  changeDisplay = () => {
    let temp = !this.state.displayLogin;
    this.setState({displayLogin: temp});
  };

  handleLoginUserUsernameChange = (e) => { this.setState({loginUsername: e.target.value}) };
  handleLoginUserPasswordChange = (e) => { this.setState({loginPassword: e.target.value}) };
  handleSignupUserUsernameChange = (e) => { this.setState({signupUsername: e.target.value}) };
  handleSignupUserEmailChange = (e) => { this.setState({signupEmail: e.target.value}) };
  handleSignupUserPasswordChange = (e) => { this.setState({signupPassword: e.target.value}) };
  handleSignupUserPasswordChangeConf = (e) => { this.setState({signupPasswordConf: e.target.value}) };

  render() {
    return (
        <MuiThemeProviderOld theme={theme}>
          <div className="login-container">
            {
              this.state.user._id ?
                <div className="user-container">
                  <img className="profile-picture" src={ this.state.user.img_url } alt={ this.state.user.username }/>
                  <p>{ this.state.user.username }</p>
                  <img className="dropdown-arrow" src={down_arrow} alt="SMKD bottom arrow" />
                </div>
                :
                <div className="custom-button" onClick={this.handleClickOpen}>
                  <span className="circle"/>
                  <span className="text">Log in</span>
                </div>
            }
            <Dialog
                onClose={ this.handleClose }
                open={ this.state.open }
                fullWidth={ true }
                maxWidth='xs'
                style={{backgroundColor: 'transparent'}}
            >
              <div className="login-content">
                <div className="title-container">
                  <h2>Welcome !</h2>
                  <div className="logo-container">
                    <img src={logo} alt="SMKD Logo"/>
                  </div>
                </div>
                {
                  this.state.displayLogin ?
                      <div className="login-form">
                        <form>
                          <Grid container justify="center">
                            <TextField
                                value={this.state.loginUsername}
                                onChange={this.handleLoginUserUsernameChange}
                                margin="dense" id="loginUserUsername" label="Username" type="text"
                                fullWidth
                            />
                            <TextField
                                value={this.state.loginPassword}
                                onChange={this.handleLoginUserPasswordChange}
                                margin="dense" id="loginUserPassword" label="Password" type="password"
                                fullWidth
                            />
                            <div className="validate-button" onClick={this.login}>
                              <span>LOG IN</span>
                            </div>
                            <div className="err-container">
                              {this.state.loginErr ? <p>{this.state.loginErr}</p> : ''}
                            </div>
                          </Grid>
                        </form>
                      </div>
                      :
                      <div className="signUp-form">
                        <form>
                          <Grid container justify="center">
                            <TextField
                                value={this.state.signupUsername}
                                onChange={this.handleSignupUserUsernameChange}
                                margin="dense" id="signupUserUsername" label="Username" type="text"
                                fullWidth
                            />
                            <TextField
                                value={this.state.signupEmail}
                                onChange={this.handleSignupUserEmailChange}
                                margin="dense" id="signupUserEmail" label="Email" type="email"
                                fullWidth
                            />
                            <TextField
                                value={this.state.signupPassword}
                                onChange={this.handleSignupUserPasswordChange}
                                margin="dense" id="signupUserPassword" label="Password" type="password"
                                fullWidth
                            />
                            <TextField
                                value={this.state.signupPasswordConf}
                                onChange={this.handleSignupUserPasswordChangeConf}
                                margin="dense" id="signupUserPasswordConf" label="Confirm Password" type="password"
                                fullWidth
                            />
                            <div className="validate-button" onClick={this.signup}>
                              <span>SIGN UP</span>
                            </div>
                            <div className="err-container">
                              {this.state.signupErr ? <p>{this.state.signupErr}</p> : ''}
                            </div>
                          </Grid>
                        </form>
                      </div>
                }
                <div className="validate-button steam-login" onClick={this.steamLogin}>
                  <img src={steamLogo} alt={"SMKD Steam Login"}/>
                  <span>LOG IN WITH STEAM</span>
                </div>
                <div className="bottom-text">
                  {
                    this.state.displayLogin ?
                        <p>Not registered ? <span onClick={this.changeDisplay}>Sign up here</span></p>
                        :
                        <p>Already registered ? <span onClick={this.changeDisplay}>Log in here</span></p>
                  }
                </div>
              </div>
            </Dialog>
          </div>
        </MuiThemeProviderOld>
    )
  }
}

export default LoginDialog;