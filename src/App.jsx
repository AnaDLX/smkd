import React, {Component} from 'react';
import './style/css/App.css';

// Components
import Sidebar from './Sidebar/Sidebar';
import Content from './Content/Content';
import LoginDialog from './LoginDialog';

// Images
import menu from "./style/img/menu.svg";
import fullScreen from "./style/img/fullscreen.svg";
import FullScreen from 'react-full-screen';
import exitFullScreen from './style/img/exitFullscreen.svg';

// Others
import queryString from 'query-string';
import jwtDecode from 'jwt-decode';
import createMuiTheme from "@material-ui/core/es/styles/createMuiTheme";

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
  palette: {
    primary: {main: '#0073aa'},
    secondary: {A400: '#2E2E2E'},
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      sidebarShrinked: false,
      sidebarTriggered: false,
      isFull: false,
      selectedMap: {
        name: 'Cache'
      }
    }
  }

  componentDidMount() {

    function getDecodedAccessToken(token) {
      try {
        return jwtDecode(token);
      } catch (Error) {
        return null;
      }
    }
    // If there are query strings in the URL (= if the token is in the URL)
    if (this.props.location.search.length > 0) {
      const values = queryString.parse(this.props.location.search);
      const tokenDecoded = getDecodedAccessToken(values.token);

      // If the token decoding is successful (= its body is readable)
      if (tokenDecoded !== null) {
        const user = tokenDecoded.body;
        console.log(user);
        this.setState({currentUser: user});
      }
    }
  }

  updateContent = (value) => {
    let temp = { ...this.state.selectedMap };
    temp.name = value;

    // get new map data

    this.setState({ selectedMap: temp });
  };

  fullScreen = () => {
    let temp = this.state.isFull;
    temp = !temp;
    this.setState({isFull: temp});
  };

  shrinkSidebar = () => {
    let temp = this.state.sidebarShrinked;
    temp = !temp;
    this.setState({sidebarShrinked: temp});
  };

  triggerSidebar = () => {
    let temp = this.state.sidebarTriggered;
    temp = !temp;
    this.setState({sidebarTriggered: temp});
  };

  render() {
    return (
      <FullScreen
        enabled={this.state.isFull}
        onChange={isFull => this.setState({isFull})}
      >
        <div className="App">
          <Sidebar
            shrink={this.state.sidebarShrinked}
            trigger={this.state.sidebarTriggered}
            updateContent={this.updateContent}
          />
          <div className={"Navbar " + (this.state.sidebarShrinked ? 'NavbarShrinked' : '')}>
            <div className="sidebar-shrinker">
              <img src={menu} alt="SMKD menu" onClick={this.shrinkSidebar}/>
            </div>
            <div className="sidebar-trigger">
              <img src={menu} alt="SMKD menu" onClick={this.triggerSidebar}/>
            </div>
            <div className="fullscreen-container">
              <img src={this.state.isFull ? exitFullScreen : fullScreen} alt="SMKD fullscreen"
                   onClick={this.fullScreen}/>
            </div>
            <LoginDialog
              user={this.state.currentUser}
            />
          </div>
          <Content
            shrink={this.state.sidebarShrinked}
            selectedMap={ this.state.selectedMap }
            theme={theme}
          />
        </div>
      </FullScreen>
    );
  }
}

export default App;
